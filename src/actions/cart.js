import { ADD_TO_CART, ADD_ORDER, UPDATE_ROW, REMOVE_FROM_CART, GET_DELIVERY_TYPES, PLACE_ORDER } from './types';
import { get, post } from '../utils/methods';

export const addToCart = (row) => dispatch => {
    dispatch({
        type: ADD_TO_CART,
        payload: row
    });
}

export const addOrder = (order) => dispatch => {
    dispatch({
        type: ADD_ORDER,
        payload: order
    });
}

export const bumpAmount = (payload) => dispatch => {
    dispatch({
        type: UPDATE_ROW,
        payload: payload
    });
}

export const removeItem = (id) => dispatch => {
    dispatch({
        type: REMOVE_FROM_CART,
        payload: id
    });
}

export const getDeliveryTypes = () => dispatch => {
    get('/api/product/deliveries').then(res => {
        dispatch({
            type: GET_DELIVERY_TYPES,
            payload: res.data
        })
    }).catch(err => {
        console.log(err);
    })
}

export const placeOrder = (order) => dispatch => {
    post('/api/orders', order).then(res => {
        dispatch({
            type: PLACE_ORDER
        })
        console.log(res);
    }).catch(err => {
        console.log(err);
    })
}