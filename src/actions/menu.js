import { get } from '../utils/methods';
import { GET_MENU } from './types';

export const getMenu = () => async dispatch => {
    get('/api/menu').then(res => {
        dispatch({
            type: GET_MENU,
            payload: res.data
        })
    }).catch(err => {
        console.log(err);
    })
}