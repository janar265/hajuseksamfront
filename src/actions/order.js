import { GET_ORDERS } from './types';
import { get, post } from '../utils/methods';

export const getOrders = (admin) => dispatch => {
    if (admin) {
        get('/api/orders/admin').then(res => {
            dispatch({
                type: GET_ORDERS,
                payload: res.data
            });
        }).catch(err => {
            console.log(err);
        })
    } else {
        get('/api/orders').then(res => {
            dispatch({
                type: GET_ORDERS,
                payload: res.data
            });
        }).catch(err => {
            console.log(err);
        })
    }
}

export const changeStatus = (payload) => dispatch => {
    post('/api/orders/status', payload).then(res => {
        console.log(res.data);
    }).catch(err => {
        console.log(err);
    })
}