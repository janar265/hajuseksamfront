export const SET_ALERT = 'SET_ALERT';
export const REMOVE_ALERT = 'REMOVE_ALERT';
export const REMOVE_ALL_ALERTS = 'REMOVE_ALL_ALERTS';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAIL = 'REGISTER_FAIL';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';
export const USER_LOADED = 'USER_LOADED';
export const AUTH_ERROR = 'AUTH_ERROR';
export const LOGOUT = 'LOGOUT';
export const GET_MENU = 'GET_MENU';
export const ADD_ORDER = 'ADD_ORDER';
export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const UPDATE_ROW = 'UPDATE_ROW';
export const REMOVE_ITEM = 'REMOVE_ITEM';
export const GET_DELIVERY_TYPES = 'GET_DELIVERY_TYPES';
export const PLACE_ORDER = 'PLACE_ORDER';
export const GET_ORDERS = 'GET_ORDERS';
export const CLEAR_CART = 'CLEAR_CART';

// const orderGET = {
//     Comment,
//     DeliveryType,
//     Price,
//     OrderStatus,
//     OrderRows: [
//         {
//             ProductName,
//             Amount,
//             Price,
//             Toppings: [
//                 {
//                     ProductName,
//                     Amount,
//                     Price
//                 }
//             ]
//         },
//         {
//             ProductName,
//             Amount,
//             Price,
//             Toppings: null
//         }
//     ]
// }