import { REGISTER_SUCCESS, REGISTER_FAIL, LOGIN_SUCCESS, LOGIN_FAIL, USER_LOADED, AUTH_ERROR, LOGOUT } from './types';
import { post, get } from '../utils/methods';
import { setAuthToken } from '../utils/setAuthToken';
import jwt_decode from 'jwt-decode';

export const loadUser = () => async dispatch => {
    if (localStorage.token) {
        setAuthToken(localStorage.token);
    }
    get('/api/account/auth').then(res => {
        console.log(res.data);
        const userClaims = jwt_decode(res.data.token);
        const userName = userClaims['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name'];
        const roles = userClaims['http://schemas.microsoft.com/ws/2008/06/identity/claims/role'];
        const user = { userName, roles };
        dispatch({
            type: USER_LOADED,
            payload: user
        })
    }).catch(err => {
        dispatch({
            type: AUTH_ERROR
        })
    })
}

export const register = ({ email, password }) => async dispatch => {
    const body = JSON.stringify({ email, password });
    post('/api/account/register', body).then(res => {
        dispatch({
            type: REGISTER_SUCCESS,
            payload: res.data
        })
        dispatch(loadUser());
    }).catch(err => {
        dispatch({
            type: REGISTER_FAIL
        })
    });
}

export const login = ({ email, password }) => async dispatch => {
    const body = JSON.stringify({ email, password });
    post('/api/account/login', body).then(res => {
        dispatch({
            type: LOGIN_SUCCESS,
            payload: res.data
        })
        console.log(res.data);
        dispatch(loadUser());
    }).catch(err => {
        console.log(err);
        dispatch({
            type: LOGIN_FAIL
        })
    });
}

export const logout = () => dispatch => {
    dispatch({
        type: LOGOUT
    })
}