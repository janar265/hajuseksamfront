import React, { Fragment, useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';
import Login from './components/auth/Login';
import Register from './components/auth/Register';
import Navbar from './components/layout/Navbar';
import Landing from './components/layout/Landing';
import Menu from './components/menu/Menu';
import Order from './components/order/Order';
import MyOrders from './components/my-orders/MyOrders';
import { loadUser } from './actions/auth';
import { setAuthToken } from './utils/setAuthToken';
import './App.css';


if (localStorage.token) {
  setAuthToken(localStorage.token);
}

const App = () => {
  useEffect(() => {
    store.dispatch(loadUser());
  }, []);
  return (
    <Provider store={store}>
      <Router>
        <Fragment>
          <Navbar />
          <Route exact path="/" component={Menu} />
          <Switch>
            <Route exact path="/register" component={Register} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/shop" component={Landing} />
            <Route exact path="/order" component={Order} />
            <Route exact path="/orders" component={MyOrders} />
          </Switch>
        </Fragment>
      </Router>
    </Provider>
  )
};

export default App;
