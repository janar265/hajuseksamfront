import { GET_MENU } from '../actions/types';

const initialState = {
    menu: null
}

export default function (state = initialState, action) {
    const { type, payload } = action;
    switch (type) {
        case GET_MENU:
            return {
                ...state,
                menu: payload
            }
        default:
            return state;
    }
}