import { ADD_TO_CART, REMOVE_FROM_CART, ADD_ORDER, UPDATE_ROW, GET_DELIVERY_TYPES, PLACE_ORDER } from '../actions/types';

const initialState = {
    isFinished: true,
    order: JSON.parse(localStorage.getItem('cart'))
}

export default function (state = initialState, action) {
    const { type, payload } = action;
    switch (type) {
        case ADD_ORDER:
            localStorage.setItem('cart', JSON.stringify(state.order));
            return {
                ...state,
                order: payload,
                isFinished: false
            }
        case ADD_TO_CART:
            localStorage.setItem('cart', JSON.stringify(state.order));
            return {
                ...state,
                order: {
                    ...state.order,
                    OrderRows: [...state.order.OrderRows, payload]
                }
            }
        case UPDATE_ROW:
            const { OrderRows } = state.order;

            const newOrderRows = [...OrderRows];
            if (payload.rowId !== -1) {
                newOrderRows[payload.rowId].Amount = payload.Amount;
                newOrderRows[payload.rowId].Price = payload.Price * payload.Amount;
            }
            return {
                ...state,
                order: {
                    ...state.order,
                    OrderRows: newOrderRows
                }
            }
        case REMOVE_FROM_CART:
            let rows = state.order.OrderRows;
            let newRows = [...rows];
            let idx = newRows.findIndex(r => r.Id === payload);
            newRows.splice(idx, 1);
            return {
                ...state,
                order: {
                    ...state.order,
                    OrderRows: newRows
                }
            }
        case GET_DELIVERY_TYPES:
            return {
                ...state,
                deliveries: payload
            }
        case PLACE_ORDER:
            localStorage.removeItem('cart');
            return {
                ...state,
                order: null,
                isFinished: true
            }
        default:
            return state;
    }
}