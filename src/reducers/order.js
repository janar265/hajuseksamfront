import { GET_ORDERS } from '../actions/types';

const initialState = {
    orders: null
}

export default function (state = initialState, action) {
    const { type, payload } = action;
    switch (type) {
        case GET_ORDERS:
            return {
                ...state,
                orders: payload
            }
        default:
            return state;
    }
}