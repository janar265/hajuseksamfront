import axios from 'axios';

const config = {
    headers: {
        'Content-Type': 'application/json'
    }
}

export const post = (endpoint, body) => {
    return axios.post(`https://localhost:5001${endpoint}`, body, config);
};

export const get = (endpoint) => {
    return axios.get(`https://localhost:5001${endpoint}`);
}