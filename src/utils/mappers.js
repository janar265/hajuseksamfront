export const productTypeMapper = (i) => {
    switch (i) {
        case 0:
            return "Pizza"
        case 1:
            return "Product"
        case 2:
            return "Topping"
        case 3:
            return "Delivery"
        default:
            return "Unknown"
    }
}

export const orderStatusMapper = (i) => {
    switch (i) {
        case 0:
            return "Waiting"
        case 1:
            return "Paid"
        case 2:
            return "Working"
        case 3:
            return "Done"
        default:
            return "Unknown"
    }
}