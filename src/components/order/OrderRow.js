import React, { useState } from 'react'
import { Button, Table } from 'semantic-ui-react';

const OrderRow = ({ row: { ProductId, name, Amount, Price, Toppings }, removeRow }) => {
    const [open, setOpen] = useState(false);
    return (
        <Table celled striped>
            <Table.Header>
                <Table.Row onClick={() => setOpen(!open)}>
                    <Table.HeaderCell className="cell">{name}</Table.HeaderCell>
                    <Table.HeaderCell className="cell">{Amount} pcs</Table.HeaderCell>
                    <Table.HeaderCell className="cell">{Number(Price).toFixed(2)} € {Toppings && `tap to view extras`}</Table.HeaderCell>
                    <Table.HeaderCell className="close-cell">
                        <Button className="square-btn" color='red' icon="close" onClick={() => removeRow(ProductId)}></Button>
                    </Table.HeaderCell>
                </Table.Row>
            </Table.Header>
            <Table.Body>
                {Toppings && open && Toppings.map(t => {
                    return <Table.Row key={t.id}>
                        <Table.Cell></Table.Cell>
                        <Table.Cell></Table.Cell>
                        <Table.Cell>{t.name}</Table.Cell>
                        <Table.Cell>{t.amount}</Table.Cell>
                    </Table.Row>
                })}
            </Table.Body>
        </Table >
    )
}

export default OrderRow
