import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { Container, Button, Dropdown, TextArea } from 'semantic-ui-react';
import OrderRow from './OrderRow';
import { removeItem, getDeliveryTypes, placeOrder } from '../../actions/cart';
import uuid from 'uuid';

const Order = ({ cart, removeItem, getDeliveryTypes, placeOrder }) => {
    useEffect(() => { getDeliveryTypes() }, []);
    const [comment, setCommentValue] = useState('');
    const [dropdownValue, setDropdownValue] = useState();

    if (cart.isFinished && !cart.OrderRows) {
        return <Redirect to="/orders" />
    }

    const deliveryTypes = cart.deliveries && cart.deliveries.map(d => {
        return { key: d.id, value: d.id, text: d.name }
    });

    const removeRow = (id) => {
        removeItem(id);
    }

    const confirmOrder = () => {
        const { order } = { ...cart }
        order.Comment = comment;
        order.Delivery.Id = dropdownValue;
        placeOrder(order);
    }


    const total = cart.order && cart.order.OrderRows.reduce((acc, r) => acc + r.Price, 0)


    return (
        <section className='landing'>
            <div className="dark-overlay">
                <Container className="container">
                    {cart.order && cart.order.OrderRows.map(r => {
                        return <OrderRow key={`${uuid.v4()}`} row={r} removeRow={removeRow} />
                    })}
                    <TextArea className="comment-box" placeholder='Comment...' value={comment} onChange={(e, data) => setCommentValue(data.value)} />
                    <Dropdown search selection placeholder='How you would receive your order?' options={deliveryTypes != null ? deliveryTypes : null} value={dropdownValue} onChange={(e, data) => setDropdownValue(data.value)} />
                    <Button className="confirm-btn" fluid color='green' onClick={() => confirmOrder()}>{`Confirm ${Number(total).toFixed(2)} €`}</Button>
                </Container>
            </div>
        </section>
    )
}

const mapStateToProps = state => ({
    cart: state.cart
})

export default connect(mapStateToProps, { removeItem, getDeliveryTypes, placeOrder })(Order)
