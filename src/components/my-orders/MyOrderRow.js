import React, { useState } from 'react'
import { Table } from 'semantic-ui-react';
import { orderStatusMapper } from '../../utils/mappers';
import MyRow from './MyRow';
import { changeStatus } from '../../actions/order';
import { connect } from 'react-redux';
import { Dropdown } from 'semantic-ui-react';

const MyOrderRow = ({ row: { id, comment, deliveryType, price, orderRows, orderStatus }, auth: { loading, user }, changeStatus }) => {
    const [open, setOpen] = useState(false);
    const [dropdownValue, setDropdownValue] = useState(orderStatus);
    const dropdownOptions = (
        [{ key: 2, value: 2, text: orderStatusMapper(2) },
        { key: 3, value: 3, text: orderStatusMapper(3) },
        { key: 0, value: 0, text: orderStatusMapper(0) },
        { key: 1, value: 1, text: orderStatusMapper(1) }]
    );

    const dropDownChanges = (value) => {
        console.log(value);
        setDropdownValue(value);
        changeStatus({ id, value });
    }
    return (
        <Table celled striped>
            <Table.Header>
                <Table.Row onClick={() => setOpen(!open)}>
                    <Table.HeaderCell className="cell">{comment}</Table.HeaderCell>
                    <Table.HeaderCell className="cell">{deliveryType}</Table.HeaderCell>
                    <Table.HeaderCell colSpan="2" className="cell">{Number(price).toFixed(2)} € {orderRows && 'tap to view'}</Table.HeaderCell>
                    <Table.HeaderCell className="close-cell">{!loading && user.roles == 'Administrator' ? <Dropdown selection options={dropdownOptions} value={dropdownValue} onChange={(e, data) => dropDownChanges(data.value)} /> : orderStatusMapper(orderStatus)}</Table.HeaderCell>
                </Table.Row>
            </Table.Header>
            <Table.Body>
                {orderRows && open && orderRows.map(or => {
                    return <MyRow row={or} />
                })}
            </Table.Body>
        </Table >
    )
}

const mapStateToProps = state => ({
    auth: state.auth
})

export default connect(mapStateToProps, { changeStatus })(MyOrderRow)
