import React, { useState, Fragment } from 'react'
import { Table } from 'semantic-ui-react';
import uuid from 'uuid';

const MyRow = ({ row: { productName, amount, price, extraToppings } }) => {
    const [openExtra, setOpenExtra] = useState(false);
    return (<Fragment>
        <Table.Row key={uuid.v4()} onClick={() => setOpenExtra(!openExtra)}>
            <Table.Cell>{productName}</Table.Cell>
            <Table.Cell>{amount} pcs</Table.Cell>
            <Table.Cell colSpan="3">{Number(price).toFixed(2)} € {extraToppings.length > 0 && 'Tap to view extra ingredients'}</Table.Cell>
        </Table.Row>
        {extraToppings && openExtra && extraToppings.map(ex => {
            return <Table.Row key={uuid.v4()}>
                <Table.Cell></Table.Cell>
                <Table.Cell>{ex.name} pcs</Table.Cell>
                <Table.Cell colSpan="3">{Number(ex.price).toFixed(2)} €</Table.Cell>
            </Table.Row>
        })}
    </Fragment>
    )
}

export default MyRow
