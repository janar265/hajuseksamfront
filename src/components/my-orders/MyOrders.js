import React, { useEffect } from 'react'
import { connect } from 'react-redux';
import { Container } from 'semantic-ui-react';
import { getOrders } from '../../actions/order';
import MyOrderRow from './MyOrderRow';

const MyOrders = ({ getOrders, order, auth: { user } }) => {
    const admin = user && user.roles === 'Administrator';
    useEffect(() => { getOrders(admin) }, []);

    return (
        <section className='landing'>
            <div className="dark-overlay">
                <Container className="container">
                    {order.orders && order.orders.map(r => {
                        return <MyOrderRow key={`${r.id}`} row={r} />
                    })}
                </Container>
            </div>
        </section>
    )
}

const mapStateToProps = state => ({
    order: state.order,
    auth: state.auth
});

export default connect(mapStateToProps, { getOrders })(MyOrders)
