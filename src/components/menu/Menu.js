import React, { useEffect } from 'react'
import { connect } from 'react-redux';
import { getMenu } from '../../actions/menu';
import { Table } from 'semantic-ui-react'

const Menu = ({ getMenu, menu }) => {
    useEffect(() => { getMenu() }, []);
    console.log(menu);
    return (
        <section className="landing">
            <div className="dark-overlay">
                <div className="landing-inner">
                    <Table singleLine inverted>
                        <Table.Body>
                            <Table.HeaderCell colSpan='2'>Pitsad</Table.HeaderCell>
                            {menu && menu.filter(i => i.productType === 0).map(pizza => {
                                return <Table.Row key={pizza.name}>
                                    <Table.Cell>{pizza.name}</Table.Cell>
                                    <Table.Cell>{Number(pizza.price).toFixed(2)} €</Table.Cell>
                                </Table.Row>
                            }
                            )}
                            <Table.HeaderCell colSpan='2'>Lisandid</Table.HeaderCell>
                            {menu && menu.filter(i => i.productType === 1).map(topping => {
                                return <Table.Row key={topping.name}>
                                    <Table.Cell>{topping.name}</Table.Cell>
                                    <Table.Cell>{Number(topping.price).toFixed(2)} €</Table.Cell>
                                </Table.Row>
                            }
                            )}
                            <Table.HeaderCell colSpan='2'>Produktid</Table.HeaderCell>
                            {menu && menu.filter(i => i.productType === 2).map(product => {
                                return <Table.Row key={product.name}>
                                    <Table.Cell>{product.name}</Table.Cell>
                                    <Table.Cell>{Number(product.price).toFixed(2)} €</Table.Cell>
                                </Table.Row>
                            }
                            )}
                        </Table.Body>
                    </Table>
                </div>
            </div>
        </section>
    )
}

const mapStateToprops = state => ({
    menu: state.menu.menu
})

export default connect(mapStateToprops, { getMenu })(Menu)
