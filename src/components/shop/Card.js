import React from 'react'
import pizza from '../../img/pizza.png';
import drink from '../../img/drink.png';
import { Button } from 'semantic-ui-react';

const Card = ({ id, name, productType, price, addToCart }) => {
    return (
        <div className="product-card">
            <img alt="pizza" className="pizza-image" src={productType === 0 ? pizza : drink} />
            <h2>{name}</h2>
            <Button color='green' onClick={(e) => addToCart(id, name, price, productType)}>{`ADD ${Number(price).toFixed(2)} €`}</Button>
        </div>
    )
}

export default Card;
