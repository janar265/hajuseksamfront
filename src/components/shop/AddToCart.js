import React, { useState } from 'react'
import { Modal, Button, Divider } from 'semantic-ui-react';
import pizza from '../../img/pizza.png';

const AddToCart = ({ close, product: { name, Price, productType }, toppings, addTopping, removeTopping, addToCart, addItemAmount, removeItemAmount }) => {
    const toppingC = { [name]: 1 };
    toppings.map(t => toppingC[t.name] = 0);
    const [toppingCount, setCount] = useState(toppingC);
    const clickPlus = (e, id, name, price) => {
        addTopping(id, name, price);
        setCount({ ...toppingCount, [e.target.name]: toppingCount[e.target.name] += 1 })
    }

    const clickMinus = (e, id, price) => {
        if (toppingCount[e.target.name] === 0) return;
        removeTopping(id, price);
        setCount({ ...toppingCount, [e.target.name]: toppingCount[e.target.name] -= 1 })
    }

    const clickItemPlus = (e) => {
        if (toppingCount[e.target.name] === 0) return;
        addItemAmount();
        setCount({ ...toppingCount, [e.target.name]: toppingCount[e.target.name] += 1 })
    }

    const clickItemMinus = (e) => {
        if (toppingCount[e.target.name] === 1) return;
        removeItemAmount();
        setCount({ ...toppingCount, [e.target.name]: toppingCount[e.target.name] -= 1 })
    }

    return (
        <Modal
            open={true}
            header="Customize your pizza"
            content={
                <div className="customize-pizza">
                    <img alt="pizza" className="big-pizza-image" src={pizza} />
                    <h1>{name}</h1>
                    <Divider horizontal>Add toppings</Divider>
                    <div className="toppings-list">
                        {toppings.map(i => {
                            return <Button.Group key={i.id} className="topping-group">
                                <Button
                                    color='teal'
                                    name={i.name}
                                    onClick={(e) => clickMinus(e, i.id, i.price)}>
                                    {`-  ${i.name} ${toppingCount[i.name]}`}</Button>
                                <Button
                                    color='teal'
                                    name={i.name}
                                    onClick={(e) => clickPlus(e, i.id, i.name, i.price)}>
                                    +</Button>
                            </Button.Group>
                        })}
                    </div>
                </div>
            }
            actions={[
                <Button key="closeBtn" color="red" onClick={(e) => close()}>Close</Button>,
                <Button.Group key={name} className="topping-group">
                    <Button
                        color='teal'
                        name={name}
                        onClick={(e) => clickItemMinus(e)}>
                        {`-  ${name} ${toppingCount[name]}`}</Button>
                    <Button
                        color='teal'
                        name={name}
                        onClick={(e) => clickItemPlus(e)}>
                        +</Button>
                </Button.Group>,
                <Button key="saveBtn" color="green" onClick={() => addToCart()}>{`Add to cart ${Number(Price).toFixed(2)} €`}</Button>
            ]}
        />
    )
}

export default AddToCart
