import React, { useEffect, useState } from 'react'
import Card from '../shop/Card';
import { Input, Dropdown } from 'semantic-ui-react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { getMenu } from '../../actions/menu';
import { addToCart, addOrder, bumpAmount } from '../../actions/cart';
import AddToCart from '../shop/AddToCart';

const Landing = ({ getMenu, menu, addToCart, order, addOrder, bumpAmount, isAuthenticated }) => {
    useEffect(() => { getMenu() }, []);
    const [dropdownValue, setDropdownValue] = useState(2);
    const [searchValue, setSearchValue] = useState('');
    const [addItem, setAddItem] = useState(null);
    if (!isAuthenticated) {
        return <Redirect to="/login" />
    }
    const dropdownOptions = (
        [{ key: 2, value: 2, text: 'All products' },
        { key: 0, value: 0, text: 'Pizza' },
        { key: 1, value: 1, text: 'Product' }]
    );

    //Clicking on cart shall open popup where you can press proceed there it shows the order and chance to modify it.
    const openModal = (id, name, price, productType) => {
        if (productType === 1) {
            const row = { Id: id, name, Amount: 1, Toppings: null, Price: price, productType };
            addItemToCart(row);
            return;
        }
        setAddItem({ Id: id, name, Amount: 1, Toppings: [], Price: price, productType });
    }

    const addItemAmount = () => {
        addItem.Price += (addItem.Price / addItem.Amount);
        addItem.Amount += 1;
    }

    const removeItemAmount = () => {
        addItem.Price -= (addItem.Price / addItem.Amount);
        addItem.Amount -= 1;
    }

    const addTopping = (id, name, price) => {
        if (addItem.Toppings.length > 0) {
            const idx = addItem.Toppings.findIndex(topping => topping.id === id);
            if (idx === -1) {
                addItem.Toppings.push({ id, amount: 1, name });
            } else {
                addItem.Toppings[idx].amount += 1;
            }
        } else {
            addItem.Toppings.push({ id, amount: 1, name });
        }
        addItem.Price += price * addItem.Amount;
    }

    const removeTopping = (id, price) => {
        const idx = addItem.Toppings.findIndex(topping => topping.id === id);
        if (addItem.Toppings[idx].amount === 1) {
            addItem.Toppings.splice(idx, 1);
        } else {
            addItem.Toppings.amount -= 1;
        }
        addItem.Price -= price * addItem.Amount;
    }

    const addItemToCart = (row) => {
        if (row) {
            if (order != null && order.OrderRows.filter(r => r.Id === row.Id).length > 0) {
                const rowId = order.OrderRows.findIndex(r => r.Id === row.Id);
                let am = order.OrderRows[rowId].Amount;
                bumpAmount({ rowId, Amount: am + 1, Price: row.Price });
                return;
            }
            if (order != null) {
                addToCart(row);
                return;
            }
            const newOrder = { Comment: '', Delivery: { Name: '' }, OrderRows: [] }
            newOrder.OrderRows.push(row);
            addOrder(newOrder);
        } else {
            if (order != null) {
                addToCart(addItem);
            } else {
                const newOrder = { Comment: '', Delivery: { Name: '' }, OrderRows: [] }
                newOrder.OrderRows.push(addItem);
                addOrder(newOrder);
            }
            setAddItem();
        }
    }

    return (
        <section className="landing">
            {addItem && <AddToCart
                close={setAddItem}
                product={addItem}
                toppings={menu.filter(i => i.productType === 2)}
                addTopping={addTopping}
                removeTopping={removeTopping}
                addToCart={addItemToCart}
                addItemAmount={addItemAmount}
                removeItemAmount={removeItemAmount} />}
            <div className="dark-overlay">
                <div className="landing-inner">
                    <div className="product-search">
                        <Input className="search-bar" icon='search' placeholder='Search...' value={searchValue} onChange={(e, data) => setSearchValue(data.value)} />
                        <Dropdown search selection options={dropdownOptions} value={dropdownValue} onChange={(e, data) => setDropdownValue(data.value)} />
                    </div>
                    <div className="shop-cards">
                        {menu && menu.filter(item => {
                            if (searchValue !== '' && item.productType !== 2) {
                                return item.name.toLowerCase().includes(searchValue.toLowerCase());
                            }
                            if (dropdownValue === 2) {
                                return item.productType === 0 || item.productType === 1;
                            }
                            return item.productType === dropdownValue;
                        }).map(i => {
                            return <Card key={i.id} name={i.name} id={i.id} productType={i.productType} price={i.price} addToCart={openModal} />
                        })}
                    </div>
                </div>
            </div>
        </section>
    )
}

const mapStateToProps = state => ({
    menu: state.menu.menu,
    order: state.cart.order,
    isAuthenticated: state.auth.isAuthenticated
})

export default connect(mapStateToProps, { getMenu, addToCart, addOrder, bumpAmount })(Landing)
