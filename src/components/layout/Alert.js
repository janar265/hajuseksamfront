import React, { Fragment } from 'react'
import { Message } from 'semantic-ui-react';
import { connect } from 'react-redux';

const Alert = ({ alerts }) => {
    if (alerts != null && alerts.length > 0) {
        return (
            <Message error header='There were some errors' list={[alerts.map(a => a.message)]}></Message>
        )
    } else {
        return (
            <Fragment></Fragment>
        )
    }
}

const mapStateToProps = state => ({
    alerts: state.alert
})

export default connect(mapStateToProps)(Alert)
