import React, { Fragment } from 'react'
import { Link } from 'react-router-dom';
import { Icon, Label } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { logout } from '../../actions/auth';

const Navbar = ({ auth: { isAuthenticated, loading, user }, logout, cart }) => {

    const adminLinks = (
        <ul>
            <li><Link to="/orders">Orders</Link></li>
            <li><a href="/" onClick={logout}><Icon name='log out'></Icon> Logout</a></li>
        </ul>
    )

    const cartTotal = (
        cart.order != null && cart.order.OrderRows.reduce((acc, r) => acc + r.Price, 0)
    );

    const authLinks = (
        <ul>
            <li><Link to="/">Menu</Link></li>
            <li><Link to="/orders">My orders</Link></li>
            <li><Link to="/order"><Icon name='shopping cart'></Icon>{cart.order != null && <Label color='teal'>{Number(cartTotal).toFixed(2)} €</Label>}</Link></li>
            <li><a href="/" onClick={logout}><Icon name='log out'></Icon> Logout</a></li>
        </ul>
    );

    const guestLinks = (
        <ul>
            <li><Link to="/register">Register</Link></li>
            <li><Link to="/login">Login</Link></li>
        </ul>
    );

    return (
        <nav className="navbar bg-dark">
            <h1>
                <Link to="/shop">Pizza bite</Link>
            </h1>
            {!loading && (<Fragment>{isAuthenticated && <h3>{user.userName}</h3>}</Fragment>)}
            {!loading && (<Fragment>{isAuthenticated ? user.roles === 'Administrator' ? adminLinks : authLinks : guestLinks}</Fragment>)}
        </nav>
    )
}

const mapStateToProps = state => ({
    auth: state.auth,
    cart: state.cart
})

export default connect(mapStateToProps, { logout })(Navbar)
