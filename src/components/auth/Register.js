import React, { useState } from 'react';
import { Button, Form, Grid, Header, Message, Segment } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import { setAlert } from '../../actions/alert';
import { register } from '../../actions/auth';
import Alert from '../layout/Alert';


const Register = ({ setAlert, register, isAuthenticated }) => {
    const [formData, setFormData] = useState({
        email: '',
        password: '',
        password2: ''
    });

    const { email, password, password2 } = formData;

    const onChange = e => setFormData({ ...formData, [e.target.name]: e.target.value })

    const onSubmit = async e => {
        e.preventDefault();
        if (password !== password2) {
            setAlert(`Passwords don't match`, 'danger');
        } else {
            console.log('Success');
            console.log(formData);
            register(formData);
        }
    }

    if (isAuthenticated) {
        return <Redirect to="" />
    }

    return (
        <Grid className="landing" textAlign='center' verticalAlign='middle'>
            <Grid.Column style={{ maxWidth: 450 }}>
                <Header as='h2' color='teal' textAlign='center'></Header>
                <Form size='large'>
                    <Segment stacked>
                        <Form.Input
                            fluid icon='user'
                            iconPosition='left'
                            placeholder='E-mail address'
                            name="email"
                            value={email}
                            onChange={e => onChange(e)}
                            required />
                        <Form.Input
                            fluid
                            icon='lock'
                            iconPosition='left'
                            placeholder='Password'
                            type='password'
                            name="password"
                            value={password}
                            onChange={e => onChange(e)}
                        />
                        <Form.Input
                            fluid
                            icon='lock'
                            iconPosition='left'
                            placeholder='Confirm password'
                            type='password'
                            name="password2"
                            value={password2}
                            onChange={e => onChange(e)}
                        />
                        <Button color='teal' fluid size='large' onClick={e => onSubmit(e)}>Register</Button>
                    </Segment>
                </Form>
                <Alert />
                <Message>Already have an account? <Link to='/login'>Sign In</Link></Message>
            </Grid.Column>
        </Grid>
    )
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated
})

export default connect(mapStateToProps, { setAlert, register })(Register);
